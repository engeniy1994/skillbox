<?php

// 5.4 Циклы while и do...while

// Напишем цикл while с заведомо ложным условием

while ( false ) {
    echo "Я цикл while";
}

// Напишем цикл do_ while

do {
    echo "Я цикл do_ while";
} while ( false );


// Пример программы с циклом do_ while

stream_set_blocking( STDIN, false); // STDN - указатель на поток ввода в консоли // false - не блокируем программу ожидая ввода след. клавиши


do {
  echo rand( 10000, 999999);  // генерация случайных чисел в диапазоне
  $key = ord(fgetc( STDIN ));
} while ( $key != 10  );
