<?php

// 5.3 Цикл FOREACH

$testResults = [ 'Иван' => 40, 'Сергей' => 50, 'Катя' => 90, 'Антон' => 100, 'Вера' => 60, ];

foreach ( $testResults as $key => $testResult ) {
    if ( $testResult > 70 ) {
        echo "Кандидат {$key} набрал больше 70 баллов\n";
    }
}

// Пример номер 2

$testArray = [ 1, 2, 3, ];

foreach ( $testArray as &$value ) { // зададим каждому элементу массива новое значение - например 0 (С помощью амперсанда '&')
    $value = 0;
}

print_r( $testArray );

// - Итог - применять цикл foreach нужно когда работаешь с массивом или объектом.