<?php

// Задайте переменную, содержащую слово, которое нужно зашифровать. Пусть слово будет состоять только из латинских букв в нижнем регистре.

$encWord = 'cesar';

// Задайте переменную, содержащую значение сдвига шифра.

$shift = 3;

// Алфавит

$leftAlphabetLimit = 65;
$sizeOfAlphabet = 26;

// Пустой массив

$shiftStr = [];

// Преобразуем в верхний регистр

$encWord = strtoupper($encWord);

// С помощью цикла for или while обратитесь к каждой букве слова и замените её, добавив значение сдвига к её позиции.
// Подсказка: каждому символу соответствует числовой код.
// Он может принимать значение от 0 до 255.
// Чтобы узнать числовой код, воспользуйтесь функцией ord, а чтобы найти символ, соответствующий числовому коду, используйте функцию chr.

for ($i = 0; $i < strlen($encWord); $i++) {
    $encryptedWordArray[] = chr(( ord($encWord[$i]) + $shift - $leftAlphabetLimit ) % $sizeOfAlphabet + $leftAlphabetLimit);
}

$encryptedWord = implode('', $encryptedWordArray);

// Выведите зашифрованное слово на экран.

echo "Зашифрованное слово: $encryptedWord\n";


// Создаем новый массив
$decryptedWordArray = [];

// Цикл расшифровки аналогично

for ($i = 0; $i < strlen($encryptedWord); $i++) {

    $decryptedWordArray[] = chr(( ord($encryptedWord[$i]) - $shift - $leftAlphabetLimit + $sizeOfAlphabet ) % $sizeOfAlphabet + $leftAlphabetLimit);

}

$decryptedWord = implode('', $decryptedWordArray);


// Вывод расшифрованного слова на экран.

echo "Расшифрованное слово: $decryptedWord";