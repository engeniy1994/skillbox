<?php


// 5.5 Операторы для работы в циклах: break и continue

// Задача , найти вхождение первой строки === b , и приостановить цикл c помощью break;

$testStr = 'abracadabra';

$exampleStrLen = strlen($testStr);

for ($i = 0; $i < $exampleStrLen; $i++) {
   if ( $testStr[$i] === 'b' ) {
        break;
   }
}

echo $i . PHP_EOL;

// Подсчитать сумму всех натуральных чисел, которые делятся без остатка на 5 с использованием continue;

$numbers = [ 5, 10, 20, 4, 3, 1, 74 ];
$sum = 0;

foreach ($numbers as $number) {
    if ( $number % 5 !== 0 ) {
        continue;
    }
    $sum += $number;
}

echo $sum;
