<?php


// 6.1 Понятие функции

    function calculateSum ( $a, $b )  // простой пример функции
    {
        return $a + $b;
    }

    echo calculateSum( 7, 3) . "\n"; // Ответ 10

    function calculateOperation ( $f, $j, $operation = '+' )  // простой пример функции - 2 c параметром
    {
        if ( $operation == '+' ) {
            return $f + $j;
        } elseif ( $operation == '-' ) {
            return $f - $j;
        }
    }

    echo calculateOperation( 3, 2) . "\n";



// 6.2 Параметры функции

    function displayParameters ( $a, $b, $c = 0 )  // функция работы с параметрами
    {
        echo $a . ' ' . $b . ' ' . $c . PHP_EOL;
    }

    displayParameters( 1, 2);



// 6.3 Возвращаемые значения

    // Выносим логику в функцию

    function isValidNumber ( $n )
    {
        return $n % 3 === 0;
    }

    function displayResult ( $n, $isValidNumber)
    {
        if ( $isValidNumber ) {
            echo 'Число ' . $n . ' делится на 3' . PHP_EOL;
        } else {
            echo 'Число ' . $n . ' не делится на 3' . PHP_EOL;
        }
    }

    for ( $i=1; $i <= 100; $i++ )
    {
        echo displayResult ($i, isValidNumber($i));
    }