<?php

$a = rand( 0, 1 );
$c = rand( 1, 3 );

var_dump( $a );

$b = $a == 0 ? null : $c;

var_dump($b);

switch ($b) {
    case null:
        echo 'Является null' . PHP_EOL;
        break;

    case 1:
        echo 'Значение равно 1' . PHP_EOL;
        break;

    default:
        echo 'Любое другое значение' . PHP_EOL;
        break;
}

var_dump( isset( $b ) );

$r = rand( 20, 30);

$f = $b ?? $r;

var_dump( $f );