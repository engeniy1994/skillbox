<?php

$arrayFruits = [
    'a' => 'Яблоко',
    'b' => 'Апельсин',
    'c' => 'Груша',
];

var_dump($arrayFruits);

$arrayFruitsKeys = array_keys($arrayFruits);

var_dump($arrayFruitsKeys);

$arrayFruitsString = implode( ' ', $arrayFruitsKeys);

echo $arrayFruitsString;
