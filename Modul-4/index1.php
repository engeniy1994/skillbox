<?php
// МОДУЛЬ 4.3 МАССИВЫ

//  простые массивы из 3-х элементов !
$list[0] = 'Овощи';
$list[1] = 'Фрукты';
$list[2] = 'Ягоды';

echo $list[1] . PHP_EOL;

$list2[] = 'a';
$list2[] = 'b';
$list2[] = 'c';

echo $list2[1] . PHP_EOL;

// END

// Ассоциативный массив - удобнее использовать со словами , а не с числами. Доступ по ключу. !

$list3['vegetables'] = 'Овощи';
$list3['fruits'] = 'Фрукты';
$list3['berries'] = 'Ягоды';

echo $list3['fruits'] . PHP_EOL;

// END

// Способы задания массивов !

// простой, индексируемый массив
$list4 = [1, 3, 5, 7, ];
echo $list4[2] . PHP_EOL;

// ассоциативный массив

$list5 = ['vegetables' => 'Овощи', 'fruits' => 'Фрукты', 'berries' => 'Ягоды', ];
echo $list5['fruits'] . PHP_EOL;

// END

// Размер(длина) массива - sizeOf / count !

$a = sizeOf($list4); // - подсчет элементов в массиве
echo $a . PHP_EOL;

$b = count($list5); // - подсчет элементов в массиве
echo $b . PHP_EOL;

// END

// Преобразование строк в массивы и обратно !

// Строка в массив

$str = 'Hello world';
$list6 = explode(' ', $str);
var_dump($list6);

$list7 = explode('!', $str);
var_dump($list7);


// Массив в строку

$list8 = [1, 3, 5, 7, ];
$str1 = implode(' ', $list8);
echo $str1 . PHP_EOL;

// END



// 4.4 МНОГОМЕРНЫЕ МАССИВЫ !

$list9 =  [1, 2, 3, ]; // одномерный массив
$list10 =  [4, 5, 6, ]; // одномерный массив

// многомерный массив - созданный из нескольких простых массивов
$list11 = [
    's1' => $list9,
    's2' => $list10,
    's3' => [7, 8, 9, ],
];

var_dump($list11['s3']);

var_dump($list11['s3'][0]);

$cities = [
    'russia' => [
        'msk' => 'Москва',
        'spb' => 'Питер',
    ],
    'germany' => [
        'Berlin',
        'Munchen',
    ],
];

var_dump($cities['russia']['msk']);
var_dump($cities['germany']['0']);

// END


// 4.5 ФУНКЦИИ РАБОТЫ С МАССИВАМИ !

// Разбиение массивов на ключи и значения

$list12 = [
    'a' => 'word a',
    'b' => 'word b',
    'c' => 'word c',
];

$list13 = array_keys($list12);
var_dump($list13);

$list14 = array_values($list12);
var_dump($list14);

// Слияние массивов

$list15 = [
    'a' => 'word a',
    'b' => 'word b',
];

$list16 = [
    'c' => 'word c',
    'd' => 'word d',
    'a' => 'word a2',
];

$list17 = array_merge($list15, $list16);
var_dump($list17);

// Слияние простых массивов

$list18 = ['a', 'b', 'c', ];
$list19 = ['d', 'e', 'a', ];
$list20 = array_merge($list18, $list19);
var_dump($list20);

// Слияние простых и ассоциативных массивов вместе

$list21 = [
    'a' => 'word a',
    'word b',
];

$list22 = [
    'c' => 'word c',
    'a' => 'word a2',
    'word d',
];

$list23 = array_merge($list21, $list22);
var_dump($list23);

// END

// Комбинирование ключей и значений в один массив

$keys = [
    'a',
    'b',
];

$values = [
    'word a',
    'word b',
];

$list24 = array_combine($keys, $values);
var_dump($list24);

// END

// Замена ключей и значений друг на друга

$list25 = [
    'a' => 'word a',
    'b' => 'word b',
];

$list26 = array_flip($list25);
var_dump($list26);

// END

// Развернуть массив

$list27 = ['a', 'b', 'c', 'd', ]; // - простой массив
$list28 = array_reverse($list27);
var_dump($list28);

$list29 = [
    'a' => 'word a',
    'b' => 'word b',
    'c' => 'word c',
    'd' => 'word d',
];
$list30 = array_reverse($list29);
var_dump($list30);

// Развернуть массив с сохранением ключей

$list31 = ['a', 'b', 'c', 'd', ]; // - простой массив
$list32 = array_reverse($list31, true); // сохранить ключи при развороте
var_dump($list32);

// END

// ПОИСК ЗНАЧЕНИЙ В МАССИВЕ ! in_array / array_search

$list33 = ['a', 'b', 'c', 'd', ]; // - простой массив
$hasLetter = in_array('b', $list33); // 1 значение - что ищем, 2 значение - где ищем. - существует ли значение 'b'
var_dump($hasLetter); // true || false

$list34 = ['a', 'b', 'c', 'd', ]; // - простой массив
$hasLetter2 = array_search('e', $list34); // 1 значение - что ищем, 2 значение - где ищем. - существует ли значение 'c'
var_dump($hasLetter2); // int || false

// END

// true || false с ассоциативными массивами

$list35 = [
    'a' => 'word a',
    'b' => 'word b',
];

$indexWord = array_search('word b', $list35); // - поиск значения 'word b' в массиве $list35
var_dump($indexWord); // - получен ключ существующего значения || иначе (false)








